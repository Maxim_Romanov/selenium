import allure

from selenium.webdriver.chrome.webdriver import WebDriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.wait import WebDriverWait


@allure.title('Результаты поиска более чем два')
# @allure.severity(Severity.BLOCKER)
def test_google_search():
    driver = WebDriver(executable_path='D:\Projects\QA_auto\chromedriver.exe')
    with allure.step('Переходим на страницу гугла'):
        driver.get('https://google.com')

    with allure.step('Заходим в плеймаркет'):
        search_input = driver.find_element_by_xpath("//input[@name='q']")
#        search_button = driver.find_element_by_xpath("//div[@class='FPdoLc VlcLAe']//input[@name='btnK']")
        search_input.send_keys('google market')
#       search_button.click()
        search_input.send_keys(Keys.ENTER)

    def check_result_count(driver):
        inner_search_results = driver.find_elements_by_xpath("//div[@class='srg']//div[@class='g']")
        return len(inner_search_results) >= 1

    with allure.step('Ожидаем что количество результатов больше чем два'):
        WebDriverWait(driver, 5, 0.5).until(check_result_count, 'Количество результатов менее 2')

    with allure.step('Переходим на страницу второго результата'):
        search_results = driver.find_elements_by_xpath("//div[@class='srg']//div[@class='g']")
        link = search_results[1].find_element_by_xpath('.//div//a//h3')
        link.click()

#        driver.switch_to.window(driver.window_handles[1])

    with allure.step('Проверяем что это гугломаркет(сверяем тайтл)'):
        assert driver.title == 'Google Play'


# warning -- invalid escape sequence \P   WAT??
